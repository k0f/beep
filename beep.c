#define MINIAUDIO_IMPLEMENTATION
#include "miniaudio.h"

#include <stdio.h>

int main()
{
    ma_result result;
    ma_engine engine;
	ma_sound sound;

    result = ma_engine_init(NULL, &engine);
    if (result != MA_SUCCESS) {
        return -1;
    }

   	ma_result err = ma_sound_init_from_file(&engine, "/home/kof/sound.wav", 0, NULL, NULL, &sound);

	if (err != MA_SUCCESS) {
    	return result;
	}

	ma_sound_start(&sound);

    //printf("Press Enter to quit...");
    //getchar();
    while(ma_sound_is_playing(&sound)){
    	sleep(1);
    }

    ma_engine_uninit(&engine);

    return 0;
}
